<?php
/**
 * Created by PhpStorm.
 * User: bkwiatek
 * Date: 29.06.2018
 * Time: 12:47
 */

namespace Tests;

use App\Calc;
use PHPUnit\Framework\TestCase;

/**
 * Class CalcTest
 * @package Tests
 * @coversDefaultClass \App\Calc
 */
class CalcTest extends TestCase
{

    /** @var Calc */
    private $calc;

    public function setUp()
    {
        $this->calc = new Calc();
    }

    /**
     * @covers ::add()
     */
    public function testAdd(): void
    {
        $this->assertEquals(5.0, $this->calc->add(2, 3));
        $this->assertEquals(2.0, $this->calc->add(1, 1));
    }

    /**
     * @covers ::add()
     * @dataProvider sumValues
     * @param $a
     * @param $b
     * @param $expected
     */
    public function testAdd2($a, $b, $expected)
    {
        $this->assertSame($expected, $this->calc->add($a, $b));
    }

    public function sumValues()
    {
        return [
            [1, 1, 2.0],
            [2, 3, 5.0],
            [5, 8, 13.0],
        ];
    }

    /**
     * @covers ::sub()
     */
    public function testSub(): void
    {
        $this->assertEquals(-1.0, $this->calc->sub(2, 3));
        $this->assertEquals(0, $this->calc->sub(1, 1));
    }

    /**
     * @covers ::multiply()
     */
    public function testMultiply(): void
    {
        $this->assertEquals(4.0, $this->calc->multiply(2, 2));
        $this->assertEquals(6.0, $this->calc->multiply(2, 3));
    }

    /**
     * @covers ::divide()
     */
    public function testDivide(): void
    {
        $this->assertEquals(5.0, $this->calc->divide(10, 2));
        $this->assertEquals(3.3333333333333, $this->calc->divide(10, 3));
    }

    /**
     * @covers ::divide()
     */
    public function testDivideByZero(): void
    {
        $this->expectExceptionMessage('Division by zero is forbidden');
        $this->calc->divide(10, 0);
    }

    /**
     * @covers ::power()
     */
    public function testPower(): void
    {
        $this->assertEquals(1024, $this->calc->power(2, 10));
        $this->assertEquals(1, $this->calc->power(2, 0));
        $this->assertEquals(0.25, $this->calc->power(2, -2));
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: bkwiatek
 * Date: 29.06.2018
 * Time: 13:22
 */

namespace Tests;

use App\BowlingGame;
use PHPUnit\Framework\TestCase;

/**
 * Class BowlingGameTest
 * @package Tests
 * @coversDefaultClass \App\BowlingGame
 */
class BowlingGameTest extends TestCase
{
    /** @var BowlingGame */
    private $bowlingGame;

    protected function setUp()
    {
        $this->bowlingGame = new BowlingGame();
    }

    /**
     * @covers ::__construct
     * @covers ::addRoll
     */
    public function testExceptionRollValueToHigh(): void
    {
        $this->expectExceptionMessage('Single bowling pins taken is to high.');
        $this->bowlingGame->addRoll(12);
    }

    /**
     * @covers ::__construct
     * @covers ::addRoll
     */
    public function testExceptionRollValueToLow(): void
    {
        $this->expectExceptionMessage('Single bowling pins taken is to low.');
        $this->bowlingGame->addRoll(-1);
    }

    /**
     * @covers ::__construct
     * @covers ::addRoll
     */
    public function testExceptionRollValueNotInteger(): void
    {
        $this->expectExceptionMessage('Argument 1 passed to App\BowlingGame::addRoll() must be of the type integer, string given');
        $this->bowlingGame->addRoll('one');
    }

    /**
     * @covers ::__construct
     * @covers ::addRoll
     */
    public function testExceptionToManyThrowsInTheGame(): void
    {
        $scores = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        foreach ($scores as $rollScore) {
            $this->bowlingGame->addRoll($rollScore);
        }
        $this->expectExceptionMessage('To many rolls in the game.');
        $this->bowlingGame->addRoll(1);
    }

    /**
     * @covers ::addRoll()
     * @covers \App\BowlingGame::__construct()
     * @covers \App\BowlingGame::getScore()
     * @covers \App\BowlingGame::calculateFinalScore()
     * @covers \App\BowlingGame::isStrike()
     * @covers \App\BowlingGame::isSpare()
     * @covers \App\BowlingGame::getFrameScore()
     * @covers \App\BowlingGame::addScore()
     */
    public function testGutterGame(): void
    {
        $scores = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $this->gameRollsTest($scores, 0);
    }

    /**
     * @covers ::addRoll()
     * @covers \App\BowlingGame::__construct()
     * @covers \App\BowlingGame::getScore()
     * @covers \App\BowlingGame::calculateFinalScore()
     * @covers \App\BowlingGame::isStrike()
     * @covers \App\BowlingGame::isSpare()
     * @covers \App\BowlingGame::getFrameScore()
     * @covers \App\BowlingGame::addScore()
     */
    public function testAllOnes(): void
    {
        $scores = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        $this->gameRollsTest($scores, 20);
    }

    /**
     * @covers ::addRoll()
     * @covers \App\BowlingGame::__construct()
     * @covers \App\BowlingGame::getScore()
     * @covers \App\BowlingGame::calculateFinalScore()
     * @covers \App\BowlingGame::isStrike()
     * @covers \App\BowlingGame::getStrikeBonus()
     * @covers \App\BowlingGame::getStrikeScore()
     * @covers \App\BowlingGame::isSpare()
     * @covers \App\BowlingGame::getSpareBonus()
     * @covers \App\BowlingGame::getSpareScore()
     * @covers \App\BowlingGame::getFrameScore()
     * @covers \App\BowlingGame::addScore()
     */
    public function testOneSpare(): void
    {
        $this->bowlingGame->addRoll(5);
        $this->bowlingGame->addRoll(5);
        $this->bowlingGame->addRoll(3);
        $this->bowlingGame->addRoll(0);
        $this->assertSame(16, $this->bowlingGame->getScore());
    }

    /**
     * @covers ::addRoll()
     * @covers \App\BowlingGame::__construct()
     * @covers \App\BowlingGame::getScore()
     * @covers \App\BowlingGame::calculateFinalScore()
     * @covers \App\BowlingGame::isStrike()
     * @covers \App\BowlingGame::getStrikeBonus()
     * @covers \App\BowlingGame::getStrikeScore()
     * @covers \App\BowlingGame::isSpare()
     * @covers \App\BowlingGame::getFrameScore()
     * @covers \App\BowlingGame::addScore()
     */
    public function testFinalValueStrike14(): void
    {
        $rolls = [10, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $this->gameRollsTest($rolls, 14);
    }

    /**
     * @covers ::addRoll()
     * @covers \App\BowlingGame::__construct()
     * @covers \App\BowlingGame::getScore()
     * @covers \App\BowlingGame::calculateFinalScore()
     * @covers \App\BowlingGame::isStrike()
     * @covers \App\BowlingGame::getStrikeBonus()
     * @covers \App\BowlingGame::getStrikeScore()
     * @covers \App\BowlingGame::isSpare()
     * @covers \App\BowlingGame::getFrameScore()
     * @covers \App\BowlingGame::addScore()
     */
    public function testFinalValueStrike28(): void
    {
        $rolls = [10, 1, 1, 10, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $this->gameRollsTest($rolls, 28);
    }

    /**
     * @covers ::addRoll()
     * @covers \App\BowlingGame::__construct()
     * @covers \App\BowlingGame::getScore()
     * @covers \App\BowlingGame::calculateFinalScore()
     * @covers \App\BowlingGame::isStrike()
     * @covers \App\BowlingGame::getStrikeBonus()
     * @covers \App\BowlingGame::getStrikeScore()
     * @covers \App\BowlingGame::isSpare()
     * @covers \App\BowlingGame::getFrameScore()
     * @covers \App\BowlingGame::addScore()
     */
    public function testFinalValue30(): void
    {
        $this->gameRollsTest([10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 30);
    }

    /**
     * @covers ::addRoll()
     * @covers \App\BowlingGame::__construct()
     * @covers \App\BowlingGame::getScore()
     * @covers \App\BowlingGame::calculateFinalScore()
     * @covers \App\BowlingGame::isStrike()
     * @covers \App\BowlingGame::getStrikeBonus()
     * @covers \App\BowlingGame::getStrikeScore()
     * @covers \App\BowlingGame::isSpare()
     * @covers \App\BowlingGame::getFrameScore()
     * @covers \App\BowlingGame::addScore()
     */
    public function testFinalValue60(): void
    {
        $this->gameRollsTest([10, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 60);
    }

    /**
     * @covers ::addRoll()
     * @covers \App\BowlingGame::__construct()
     * @covers \App\BowlingGame::getScore()
     * @covers \App\BowlingGame::calculateFinalScore()
     * @covers \App\BowlingGame::isSpare()
     * @covers \App\BowlingGame::getSpareBonus()
     * @covers \App\BowlingGame::getSpareScore()
     * @covers \App\BowlingGame::isStrike()
     * @covers \App\BowlingGame::getStrikeBonus()
     * @covers \App\BowlingGame::getStrikeScore()
     * @covers \App\BowlingGame::getFrameScore()
     * @covers \App\BowlingGame::addScore()
     */
    public function testOneStrike(): void
    {
        $this->bowlingGame = new BowlingGame();
        $this->bowlingGame->addRoll(10);
        $this->bowlingGame->addRoll(3);
        $this->bowlingGame->addRoll(4);
        $this->rollMany(16, 0);
        $this->assertSame(24, $this->bowlingGame->getScore());
    }

    /**
     * @coversNothing
     */
    public function testAllTens(): void
    {
        $rolls = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,];
        $this->gameRollsTest($rolls, 300);
    }

    /**
     * @covers ::addRoll()
     * @covers \App\BowlingGame::__construct()
     * @covers \App\BowlingGame::getScore()
     * @covers \App\BowlingGame::calculateFinalScore()
     * @covers \App\BowlingGame::isSpare()
     * @covers \App\BowlingGame::getSpareBonus()
     * @covers \App\BowlingGame::getSpareScore()
     * @covers \App\BowlingGame::isStrike()
     * @covers \App\BowlingGame::getStrikeBonus()
     * @covers \App\BowlingGame::getStrikeScore()
     * @covers \App\BowlingGame::getFrameScore()
     * @covers \App\BowlingGame::addScore()
     */
    public function testAllTens2(): void
    {
        $this->rollMany(12, 10);
        $this->assertSame(300, $this->bowlingGame->getScore());
    }

    /**
     * @covers ::addRoll()
     * @covers \App\BowlingGame::__construct()
     * @covers \App\BowlingGame::getScore()
     * @covers \App\BowlingGame::getRollScore()
     * @covers \App\BowlingGame::calculateFinalScore()
     * @covers \App\BowlingGame::isSpare()
     * @covers \App\BowlingGame::getSpareScore()
     * @covers \App\BowlingGame::getSpareBonus()
     * @covers \App\BowlingGame::isStrike()
     * @covers \App\BowlingGame::getStrikeBonus()
     * @covers \App\BowlingGame::getStrikeScore()
     * @covers \App\BowlingGame::getFrameScore()
     * @covers \App\BowlingGame::addScore()
     */
    public function testRollScores(): void
    {
        # frame 1
        $this->bowlingGame->addRoll(5);
        $this->assertSame(5, $this->bowlingGame->getRollScore());
        $this->bowlingGame->addRoll(5);
        $this->assertSame(10, $this->bowlingGame->getRollScore());
        # frame 2
        $this->bowlingGame->addRoll(5);
        $this->assertSame(20, $this->bowlingGame->getRollScore());
        $this->bowlingGame->addRoll(5);
        $this->assertSame(25, $this->bowlingGame->getRollScore());
        # frame 3
        $this->bowlingGame->addRoll(2);
        $this->assertSame(29, $this->bowlingGame->getRollScore());
        $this->bowlingGame->addRoll(0);
        $this->assertSame(29, $this->bowlingGame->getRollScore());
        # frame 4
        $this->bowlingGame->addRoll(10);
        $this->assertSame(39, $this->bowlingGame->getRollScore());
        # frame 5
        $this->bowlingGame->addRoll(5);
        $this->assertSame(49, $this->bowlingGame->getRollScore());
        $this->bowlingGame->addRoll(5);
        $this->assertSame(59, $this->bowlingGame->getRollScore());
    }

    private function gameRollsTest(array $scores, int $expectedFinalScore): void
    {
        foreach ($scores as $rollScore) {
            $this->bowlingGame->addRoll($rollScore);
        }
        $this->assertSame($expectedFinalScore, $this->bowlingGame->getScore());
    }

    private function rollMany($n, $pin): void
    {
        for ($i = 0; $i < $n; $i++) {
            $this->bowlingGame->addRoll($pin);
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: bkwiatek
 * Date: 29.06.2018
 * Time: 13:01
 */

namespace App;

class Calc
{
    public function add(float $a, float $b): float
    {
        return $a + $b;
    }

    public function sub(float $a, float $b): float
    {
        return $a - $b;
    }

    public function multiply(float $a, float $b): float
    {
        return $a * $b;
    }

    public function divide(float $a, float $b): float
    {
        if ($b === 0.0) {
            throw new \Exception('Division by zero is forbidden');
        }
        return $a / $b;
    }

    public function power(float $a, int $b): float
    {
        return pow($a, $b);
    }
}

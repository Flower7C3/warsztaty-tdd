<?php
/**
 * Created by PhpStorm.
 * User: bkwiatek
 * Date: 29.06.2018
 * Time: 13:33
 */

namespace App;

class BowlingGame
{
    private const ROLL_PINS_MIN = 0;
    private const ROLL_PINS_MAX = 10;
    private const MAX_ROLLS = 21;
    private const GAME_FRAMES = 10;

    /** @var int */
    private $rollsAmount = 0;

    /** @var array */
    private $rolls;

    /** @var int|null */
    private $score;

    public function __construct()
    {
        $this->rolls = array_fill(0, self::MAX_ROLLS, 0);
    }

    /**
     * @param int $pins
     */
    public function addRoll(int $pins): void
    {
        if ($pins > self::ROLL_PINS_MAX) {
            throw new \InvalidArgumentException('Single bowling pins taken is to high.');
        }
        if ($pins < self::ROLL_PINS_MIN) {
            throw new \InvalidArgumentException('Single bowling pins taken is to low.');
        }
        if (!isset($this->rolls[$this->rollsAmount + 1])) {
            throw new \InvalidArgumentException('To many rolls in the game.');
        }
        $this->rolls[$this->rollsAmount++] = $pins;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        if ($this->score === null) {
            $this->calculateFinalScore();
        }
        return $this->score;
    }

    /**
     * @return int
     */
    public function getRollScore(): int
    {
        $this->score = null;
        $score = $this->getScore();
        $this->score = null;
        return $score;
    }

    /**
     *
     */
    private function calculateFinalScore(): void
    {
        $roll = 0;
        for ($frame = 1; $frame <= self::GAME_FRAMES; $frame++) {
            if ($this->isStrike($roll)) {
                $this->getStrikeScore($roll);
            } else {
                if ($this->isSpare($roll)) {
                    $this->getSpareScore($roll);
                } else {
                    $this->getFrameScore($roll);
                }
                $roll++;
            }
            $roll++;
        }
    }

    /**
     * @param $score
     */
    private function addScore($score): void
    {
        $this->score += $score;
    }

    /**
     * @param $roll
     */
    private function getFrameScore($roll): void
    {
        $this->addScore($this->rolls[$roll] + $this->rolls[$roll + 1]);
    }

    /**
     * @param $roll
     * @return bool
     */
    private function isStrike($roll): bool
    {
        return self::ROLL_PINS_MAX === $this->rolls[$roll];
    }

    /**
     * @param $roll
     */
    private function getStrikeScore($roll): void
    {
        $this->addScore(self::ROLL_PINS_MAX + $this->getStrikeBonus($roll));
    }

    /**
     * @param $roll
     * @return int
     */
    private function getStrikeBonus($roll): int
    {
        return $this->rolls[$roll + 1] + $this->rolls[$roll + 2];
    }

    /**
     * @param $roll
     * @return bool
     */
    private function isSpare($roll): bool
    {
        return self::ROLL_PINS_MAX === $this->rolls[$roll] + $this->rolls[$roll + 1];
    }

    /**
     * @param $roll
     */
    private function getSpareScore($roll): void
    {
        $this->addScore($this->getFrameScore($roll) + $this->getSpareBonus($roll));
    }

    /**
     * @param $roll
     * @return int
     */
    private function getSpareBonus($roll): int
    {
        return $this->rolls[$roll + 2];
    }
}
